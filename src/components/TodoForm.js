import React, {useState} from 'react';
import { v4 as uuidv4}  from "uuid"; 



function TodoForm({addTodo}){

    //eğer süslü parantez yapmazsan yukarıda, tüm nesneleri tek bir prop olarak almış olacasın.
    //mesela function TodoForm(props) dersem
    //şu an addTodo'ya erişmek için props.addTodo demem gerekiyor.
    //ama function TodoForm({addTodo}) dersem direkt olarak gelen propların içindeki addTodo fonksiyonuna erişebilirsin.
    const [todo,setTodo] =useState({

        id :"",
        task:"",
        completed :false
    });

    function handleTaskInputChange(e){
        setTodo({...todo,task: e.target.value});
    }

    const HandleSubmit = (e) => {
        e.preventDefault();
        if (todo.task.trim()){
           addTodo({id: uuidv4() , task: todo.task ,completed : false });
 
           setTodo({...todo, task: "" });
        }

    };
return(
    <form onSubmit={HandleSubmit}>
        <input
        name="task"
        type="text"
        value={todo.task}
        onChange={handleTaskInputChange}
        />
        <button type= "submit"> Submit </button>
    </form>
); 
}

export default TodoForm;